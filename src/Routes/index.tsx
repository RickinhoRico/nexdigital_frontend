import React from 'react'
import { Routes, Route } from 'react-router-dom'
import { useAuth } from '../context/AuthContext'

import { Home } from '../pages/Home'
import { Login } from '../pages/Login'
import { Register } from '../pages/Register'
import { Shop } from '../pages/Shop'

export function MainRoutes(){
  const { user } = useAuth();

  return (
      <Routes>
        <Route path="/" element= { user ? <Home /> : <Login /> }  />
        <Route path="/login" element= { !user ? <Login /> : <Home /> } />
        <Route path="/register" element= { !user ? <Register /> : <Home /> } />
        <Route path="/shop" element= { user ? <Shop /> : <Login /> } />
      </Routes>
  )
}