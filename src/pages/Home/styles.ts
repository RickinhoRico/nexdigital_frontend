import styled from 'styled-components'
import { shade } from 'polished'

export const Container = styled.div`
  width: 100%;
  display:flex;
  flex-direction: column;
  align-items: center;
`

export const Content = styled.div`
  width: 100%;
  max-width: 1100px;
  padding: 1rem 3rem;
  margin-top: 2rem;
  display: flex;
  flex-direction: column;

  header{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
  
    button{
      background: var(--orange-500);
      border-radius: .3rem;
      border: none;
      padding: .5rem 1rem;
      color: var(--gray-550);
      font-size: 1.1rem;
      font-weight: bold;
      margin-top: 1rem;
      transition: background 0.2s ease;

      &:hover{
        background: ${shade(0.2, '#FF9000')}
      }
    }
  }
`

export const ContainerShops = styled.div`
  width: 100%;
  max-width: 1100px;
  border-top: 1px solid var(--gray-350);
  margin-top: 1rem;
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-around;
`

export const CardShop = styled.button`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: flex-start;
  padding: 1rem 0.5rem;
  width: 10rem;
  margin: 1rem 1rem;
  background: var(--gray-450);
  border-radius: .3rem;
  border: none;
  cursor: pointer;

  img{
    width: 6rem;
    height: 6rem;
    background: var(--white);
  }

  h3{
    color: var(--orange-500);
    font-weight: 500;
    margin-top: 0.5rem;
  }

  p{
    width: 100%;
    font-size: .8rem;
    color: var(--white);
    font-weight: 400;
    padding: 0.5rem 1rem;
  }
  
`