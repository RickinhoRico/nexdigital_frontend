/* eslint-disable */
import { useCallback, useEffect, useState } from 'react'
import ReactModal from 'react-modal'

import { Header } from '../../components/Header'
import { Container, Content, ContainerShops, CardShop } from './styles'
import { useAuth } from '../../context/AuthContext'
import { FormShopModal } from '../../components/FormShopModal'
import { api } from '../../services/api'

interface ShopProps{
  id: string,
  user_id: string,
  name: string,
  description: string,
  created_at: string,
  updated_at: string,
}

export function Home(){
  const { user, token }: any = useAuth()
  const [ dataShops, setDataShops ] = useState<ShopProps[]>([]);
  const [isOpenShopModal, setIsOpenShopModal] = useState(false);

  const getShops = useCallback( async ()=> {
      const response: any = await api.get(`/users/${user.id}/shops`, {          
        headers: {
          "authorization": `Bearer ${token}`
        }
      })

      if(response){
        setDataShops( response.data )
      }
    },[])

  useEffect(() => {
    getShops();
  }, [user, token, dataShops, ReactModal])

  function handleOpenShopModal(){
    setIsOpenShopModal(true);
  }

  function handleCloseShopModal(){
    setIsOpenShopModal(false);    
    getShops();
  }
  return (
    <Container>
      <Header /> 
      <Content>
        <header>
          <h1>Lojas</h1>
          <button onClick={handleOpenShopModal}>Adicionar loja</button>
        </header>

        <ContainerShops >
          {dataShops.map(Shop =>(
              <CardShop>
                <img src="" alt="" />
                <h3>{Shop.name}</h3>
                <p>{Shop.description}</p>
              </CardShop>
          ))}
        </ContainerShops>
      </Content>
      <ReactModal
        isOpen={isOpenShopModal}
        onRequestClose={handleCloseShopModal}
        overlayClassName='react-modal-overlay'
        className='react-modal-content'    
      >
        <FormShopModal onCloseShopModal={handleCloseShopModal}/>
      </ReactModal>
    </Container>
  )
}