import { Link } from 'react-router-dom'
import { FiArrowLeft } from 'react-icons/fi'

import logoImg from '../../assets/logo.svg'
import { Container, Content, Background} from './styles'

import { FormRegister } from '../../components/FormRegister'

export function Register(){
  return (
    <Container>
      <Background />
      <Content>
        <img src={logoImg} alt='my t-shit' />
        <FormRegister />
        <Link to="/login">
          <FiArrowLeft />
          Voltar para login
        </Link>
      </Content>
    </Container>
  )
}