import styled from 'styled-components'
import { shade } from 'polished'

import registerBackground from '../../assets/img/bg_register.jpg'

export const Container = styled.div`
  height: 100vh;
  display: flex;
  flex-direction: row; 
  justify-content: flex-end;
  align-items: stretch;
  background: var(--gray-450);
`

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  width: 100%;
  max-width: 500px;

  img {
    width: 18rem;
  }

  a{
    display: flex;
    align-items: center;
    color: var(--white);
    text-decoration: none;    
    margin-top: 2rem;
    transition: color 0.2s;

    &:hover{
      color: ${shade(0.2, '#F4EDE8')}
    }

    svg{
      margin-right: .5rem;
    }
  }
`

export const Background = styled.div`
  flex: 1;
  background: url(${registerBackground}) no-repeat center;
  background-size: cover;
`
