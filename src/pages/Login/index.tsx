import { Link } from 'react-router-dom'
import { FiLogIn } from 'react-icons/fi'

import logoImg from '../../assets/logo.svg'
import { Container, Content, Background} from './styles'

import { FormLogin } from '../../components/FormLogin'

export function Login(){
  return (
    <Container>
      <Content>
        <img src={logoImg} alt='my t-shit' />
        <FormLogin />
        <Link to="/register">
          <FiLogIn />
          Cadastre-se
        </Link>
      </Content>
      <Background />
    </Container>
  )
}