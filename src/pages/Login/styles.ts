import styled from 'styled-components'
import { shade } from 'polished'

import loginBackground from '../../assets/img/bg_login.jpg'

export const Container = styled.div`
  height: 100vh;
  display: flex;
  align-items: stretch;
  background: var(--gray-450);
`

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  place-content: center;
  align-items: center;

  width: 100%;
  max-width: 500px;

  img {
    width: 18rem;
  }

  a{
    display: flex;
    align-items: center;
    color: var(--orange-500);
    text-decoration: none;    
    margin-top: 2rem;
    transition: color 0.2s;

    &:hover{
      color: ${shade(0.2, '#FF9000')}
    }

    svg{
      margin-right: .5rem;
    }
  }
`

export const Background = styled.div`
  flex: 1;
  background: url(${loginBackground}) no-repeat right;
  background-size: cover;
`
