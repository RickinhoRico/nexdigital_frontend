import { Header } from "./components/Header";
import { GlobalStyle } from "./styles/global";
import { BrowserRouter as Router } from 'react-router-dom'

import { AuthProvider } from './context/AuthContext'
import { MainRoutes } from './Routes'

export function App() {
  return (
    <AuthProvider>
      <Router>
        <MainRoutes />
        <GlobalStyle />
      </Router>
    </AuthProvider>
  );
}

