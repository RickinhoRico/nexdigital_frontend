import React, { createContext, useCallback, useContext, useState } from 'react'
import { api } from '../services/api'

interface User{
  id: string,
  name: string,
  email: string,
}

interface AuthState{
  token: string,
  user: User
}

interface SignInCredentials{
  email: string,
  password: string
}

interface IAuthContext {
  user: object;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
  token: string;
}

export const AuthContext = createContext<IAuthContext>({} as IAuthContext);

export const AuthProvider = ( { children }: any ) => {
  const [ data, setData ] = useState<AuthState>(() => {
    const token = localStorage.getItem('@tshit:token');
    const user = localStorage.getItem('@tshit:user');

    if( token &&  user){
      return { token, user: JSON.parse(user)}
    }

    return {} as AuthState;
  });

  const signIn = useCallback(async ({ email, password }: SignInCredentials) => {
    const response = await api.post('Authenticate', {
      email,
      password,
    })
    
    const { token, user } = response.data;

    localStorage.setItem('@tshit:token', token);
    localStorage.setItem('@tshit:user', JSON.stringify(user));

    setData({ token, user })

  }, [])

  const signOut = useCallback(async () => {    
    localStorage.removeItem('@tshit:token');
    localStorage.removeItem('@tshit:user');

    setData({} as AuthState)
  },[]);

  return (
    <AuthContext.Provider value={{ user: data.user, token: data.token, signIn, signOut}}>
      { children }
    </AuthContext.Provider>
  )
}

export function useAuth(): IAuthContext {
  const context = useContext(AuthContext);

  if(!context){
    throw new Error('useAuth must be within a AuthProvider')
  }

  return context;
}