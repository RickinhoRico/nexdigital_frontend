import styled from 'styled-components'
import { shade } from 'polished'

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  width: 380px;
  margin-top: 2rem;

  h1{
    color: var(--white);
    text-align: center;
    margin-bottom: 1rem;
  }
  
  input{
    background: var(--gray-550);
    border-radius: .5rem;
    border: 2px solid var(--gray-550);
    padding: 1rem;
    width: 100%;
    color: var(--white);
  }

  p {
    font-size: .8rem;
    color: var(--orange-500);
    margin: .4rem 1rem;
  }

  button{
    background: var(--orange-500);
    border-radius: .5rem;
    border: none;
    padding: 1rem;
    width: 100%;
    color: var(--gray-550);
    font-size: 1.2rem;
    font-weight: bold;
    margin-top: 1rem;
    transition: background 0.2s;

    &:hover{
      background: ${shade(0.2, '#FF9000')}
    }
  }
`