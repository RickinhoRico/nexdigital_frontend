import { Form } from './styles'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import { schema } from './validation'
import { api } from '../../services/api';

export function FormRegister(){
  const {register, handleSubmit, formState:{ errors }} = useForm({
    defaultValues: schema.cast(),
    resolver: yupResolver(schema, { stripUnknown: true, abortEarly: false }),
  });

  const onSubmit = async(data: any) => {
    try{
      await api.post('/users', data)
    }catch(err){
      console.log(err)
    }
  }
  return(
    <Form>
      <h1>Faça seu cadastro</h1>
      <input type="text" {...register('name')} name="name" placeholder="Nome" />
      <p>{errors.name?.message}</p>
      <input type="email" {...register('email')} name="email" placeholder="E-mail" />
      <p>{errors.email?.message}</p>
      <input type="password" {...register('password')} name="password"  placeholder='Senha' />
      <p>{errors.password?.message}</p>
      <input type="password" {...register('confirm_pw')} name="confirm_pw"  placeholder='Confirme a senha' />
      <p>{errors.confirm_pw?.message}</p>
      <button type="submit" onClick={handleSubmit(onSubmit)}>CADASTRAR</button>
    </Form>
  )
} 