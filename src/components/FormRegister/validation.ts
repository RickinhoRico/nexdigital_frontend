import * as yup from 'yup'

export const schema = yup.object().shape({
  name: yup.string()
            .required('Digite seu nome'),
  email: yup.string()
            .email('Digite um e-mail válido.')
            .required('Por favor digite um email.'),
  password: yup.string()
            .required('por favor digite uma senha.'),
  confirm_pw: yup.string()
            .test('match',
              'As senhas não são compatíveis',
              function (confirm_pw) {
                return confirm_pw === this.parent.password;
              }
            )
            .required('por favor confirme sua senha senha.')
}).required()