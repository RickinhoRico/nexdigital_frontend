import styled from 'styled-components'
import { shade } from 'polished'

export const Container= styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  h1{
    color: var(--white);
  }
  
  input[type=text]{
    background: var(--gray-550);
    border-radius: .5rem;
    border: 2px solid var(--gray-550);
    padding: .7rem;
    width: 100%;
    color: var(--white);
  }
  
  textarea{
    overflow-y: hidden;
    background: var(--gray-550);
    border-radius: .5rem;
    border: 2px solid var(--gray-550);
    padding: .7rem;
    width: 100% !important;
    height: 7.5rem !important;
    color: var(--white);
    resize: none;
  }

  p {
    font-size: .8rem;
    color: var(--orange-500);
    margin: .3rem 1rem;
  }

  button{
    background: var(--orange-500);
    border-radius: .5rem;
    border: none;
    padding: 1rem;
    width: 100%;
    color: var(--gray-550);
    font-size: 1.2rem;
    font-weight: bold;
    margin-top: 1rem;
    transition: background 0.2s ease;

    &:hover{
      background: ${shade(0.2, '#FF9000')}
    }
  }

`

export const UploadImage = styled.label`
  width: 8rem;
  height: 8rem;
  border-radius: 50%;
  background: var(--white);
  border: 2px solid var(--gray-550);
  margin: 1rem 0;
  transition: all 0.2s ease;
  cursor: pointer;

  &:hover{
    border: 2px solid ${shade(0.2, '#FF9000')}
  }

  input[type=file]{
    display: none;
  }

`