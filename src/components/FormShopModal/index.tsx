import { Container, UploadImage } from "./styles"
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import { schema } from './validation'
import { api } from "../../services/api";
import { useAuth } from "../../context/AuthContext";
import React, { ReactElement } from "react";

interface HeaderProps{
  onCloseShopModal: () => void;
}

export function FormShopModal({ onCloseShopModal }: HeaderProps){
  const { user, token }: any = useAuth();
  const {register, handleSubmit, setError, formState:{ errors }} = useForm({
    defaultValues: schema.cast(),
    resolver: yupResolver(schema, { stripUnknown: true, abortEarly: false }),
  });

  const onSubmit = async (data: any) => {
    const { name, description } = data;
    try{
      await api.post(`/users/${user.id}/shops`, { name, description, user_id: user.id}, {
        headers: {
          "authorization": `Bearer ${token}`
        }
      }).catch( function (error){
        if(error.response){
          setError('backValidate', {type:'custom', message: error.response.data.error})
        }
      });
    }catch(err : any) {
       console.log(err)
    }

  }

  return(
    <Container>
      <h1>Loja</h1>
      <UploadImage><input type="file"/></UploadImage>
      <input type="text" {...register('name')} name="name" placeholder="Nome" />
      <p>{errors.name?.message}</p>
      <textarea {...register('description')} name="description" placeholder="Descrição da loja"></textarea>
      <p>{errors.description?.message}</p>
      {errors.backValidate && <p>{errors.backValidate.message}</p>}

      <button onClick={handleSubmit(onSubmit)}>CRIAR LOJA</button>
    </Container>
  )
}