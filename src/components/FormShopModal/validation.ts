import * as yup from 'yup'

export const schema = yup.object().shape({
  name: yup.string()
          .required('Por favor digite um nome para sua loja'),
  description: yup.string()
            .max(250, 'Limite máximo de 250 caracteres')
            .required('Por favor digite uma descrição para sua loja'),
}).required()