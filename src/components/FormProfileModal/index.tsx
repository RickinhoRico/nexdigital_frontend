import { Container, UploadImage } from "./styles"
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import { schema } from './validation'
import { api } from "../../services/api";
import { useAuth } from "../../context/AuthContext";

export function FormProfileModal(){
  const { user, token }: any = useAuth();
  const {register, handleSubmit, formState:{ errors }} = useForm({
    defaultValues: schema.cast(),
    resolver: yupResolver(schema, { stripUnknown: true, abortEarly: false }),
  });

  const onSubmit = async (data: any) => {
    console.log(data);
    try{
      await api.patch(`/users/${user.id}`, data, {
        headers: {
          "authorization": `Bearer ${token}`
        }
      });
    }catch(err) {
      console.log(err);
    }
  }

  return(
    <Container>
      <h1>Perfil</h1>
      <UploadImage><input type="file"/></UploadImage>
      <input type="text" {...register('name')} name="name" placeholder="Nome" />
      <p>{errors.name?.message}</p>
      <input type="email" {...register('email')} name="email" placeholder="E-mail" />
      <p>{errors.email?.message}</p>
      <input type="password" {...register('password')} name="password" placeholder="Senha atual" />
      <p>{errors.password?.message}</p>
      <input type="password" {...register('new_password')} name="new_password" placeholder="Nova senha" />
      <p>{errors.new_password?.message}</p>
      <input type="password" {...register('confirm_pw')} name="confirm_pw" placeholder="Confirmar senha" />
      <p>{errors.confirm_pw?.message}</p>

      <button onClick={handleSubmit(onSubmit)}>SALVAR</button>
    </Container>
  )
}