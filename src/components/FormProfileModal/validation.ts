import * as yup from 'yup'

export const schema = yup.object().shape({
  name: yup.string()
          .optional(),
  email: yup.string()
            .email('Digite um e-mail válido.')
            .optional(),
  password: yup.string()
  .optional(),
  new_password: yup.string()
  .optional(),
  confirm_pw: yup.string()
          .test('match',
            'As senhas não são compatíveis',
            function (confirm_pw) {
              return confirm_pw === this.parent.new_password;
          }
  )
  .optional(),
}).required()