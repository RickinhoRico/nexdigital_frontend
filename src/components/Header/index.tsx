import { useState } from 'react'
import ReactModal from 'react-modal'
import { FiPower } from 'react-icons/fi'

import { useAuth } from '../../context/AuthContext'
import { Container, ButtonProfile, ButtonQuit, Content } from "./styles";
import { FormProfileModal } from '../FormProfileModal';
import logoImg from '../../assets/logo.svg'


export function Header(){
  const { user, signOut }: any = useAuth()
  const [isOpenProfileModal, setIsOpenProfileModal] = useState(false);

  function handleOpenProfileModal(){
    setIsOpenProfileModal(true);
  }

  function handleCloseProfileModal(){
    setIsOpenProfileModal(false);
  }

  return (
    <Container>
      <Content>
        <img src={logoImg} alt='my t-shit' />
        <header>
          <h2>Seja bem vindo</h2>
          <span>{ user.name }</span>
        </header>

        <ButtonProfile onClick={handleOpenProfileModal} />
        <ButtonQuit onClick={signOut}>
          <FiPower />
        </ButtonQuit>
      </Content>
      <ReactModal
        isOpen={isOpenProfileModal}
        onRequestClose={handleCloseProfileModal}
        overlayClassName='react-modal-overlay'
        className='react-modal-content'    
      >
        <FormProfileModal />
      </ReactModal>
    </Container>
  )
}