import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  justify-content: center;
  width: 100%;
  padding: 1rem 0;
  background: var(--gray-450);
`

export const Content = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  max-width: 1100px;
  position: relative;

  img{
    width: 12rem;
    /* margin-right: 2rem; */
  }

  header{
    align-self: flex-start;
    margin-left: -25rem;

    h2{
      color: var(--white);
    }
    
    span{
      color: var(--orange-500);
      font-size: 1.5rem;
      font-weight: bold;
    }

  }
`

export const ButtonProfile = styled.button`
  width: 8rem;
  height: 8rem;
  border-radius: 50%;
  border: 2px solid var(--gray-550);
  background: var(--white);
  position: absolute;
  top: .8rem;
  right: 7rem;
  cursor: pointer;
  transition: all .2s;

  &:hover{
    border: 2px solid var(--orange-500);
  }
`

export const ButtonQuit = styled.button`
  width: 2.5rem;
  height: 2.5rem;
  border-radius: 50%;
  background: var(--gray-450);
  color: var(--white);
  border: none;
  cursor: pointer;
  transition: all .2s;

  &:hover{
    color: var(--orange-500);
  }

  svg{
    width: 1.5rem;
    height: 1.5rem;
  }

`