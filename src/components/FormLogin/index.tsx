import { Form } from './styles'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'

import { schema } from './validation'
import { useAuth } from '../../context/AuthContext'
import { useCallback, useContext } from 'react'

interface ISignIn{
  email: string,
  password: string
}

export function FormLogin(){
  const { signIn } = useAuth()
  const {register, handleSubmit, formState:{ errors }} = useForm({
    defaultValues: schema.cast(),
    resolver: yupResolver(schema, { stripUnknown: true, abortEarly: false }),
  });

  const onSubmit = useCallback(
    (data: ISignIn) => {
      signIn({
        email: data.email,
        password: data.password
      });
    },[signIn]
  )

  return(
    <Form>
      <h1>Faça seu login</h1>
      <input type="email" {...register('email')} name="email" placeholder="E-mail" />
      <p>{errors.email?.message}</p>
      <input type="password" {...register('password')} name="password"  placeholder='Senha' />
      <p>{errors.password?.message}</p>
      <button type="submit" onClick={handleSubmit(onSubmit)}>ENTRAR</button>
    </Form>
  )
} 