import * as yup from 'yup'

export const schema = yup.object().shape({
  email: yup.string()
            .email('Digite um e-mail válido.')
            .required('Por favor digite um email.'),
  password: yup.string()
            .required('por favor digite uma senha.')
}).required()